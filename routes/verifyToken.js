const jwt =require("jsonwebtoken");

const verifyToken =(req,res,next)=>{
    const authHeader =req.headers.authorization;
    if(authHeader){
        const token = authHeader.split(" ")[1]
        jwt.verify(token,process.env.JWT_SEC,(err,groups)=>{
            if(err) res.status(403).json("token is not match");
            req.groups =groups;
          
            next();
        })
    }else{
        return res.status(401).json("you are not authenicated")
    }
};


const verifyTokenAndAuthorization = (req,res,next)=>{
    verifyToken(req,res,()=> {
        if(req.groups.id === req.params.id || req.params.isAdmin){
            next();

        }else{
            res.status(403).json("you are not allowed to do that!")
        }
    })
}


const verifyTokenAndAdmin = (req,res,next)=>{
    verifyToken(req,res,()=> {
        if(req.groups.isAdmin){
            next();

        }else{
            res.status(403).json("you are not allowed to do that!")
        }
    })
}
module.exports = { 
    verifyToken,
    verifyTokenAndAuthorization ,
    verifyTokenAndAdmin
}
