const { verify } = require("jsonwebtoken");
const { verifyToken, verifyTokenAndAuthorization, verifyTokenAndAdmin } = require("./verifyToken");
const Group = require("../models/groups");

const router = require("express").Router();


//Update
router.put("/:id", verifyTokenAndAuthorization, async (req, res) => {
    if (req.body.password) {
        req.body.password = CryptoJS.AES.encrypt(
            req.body.password,
            process.env.PASS_SEC)
            .toString();

    }
    try {
        const updatedgroups = await Groups.findByIdAndUpdate(
            req.params.id,
            {
                $set: req.body,
            },
            { new: true }
        );
        res.status(200).json(updatedgroups)
    } catch (err) {
        res.status(500).json(err);
    }

});
// delete
router.delete("/:id", verifyTokenAndAdmin, async (req, res) => {
    try {
        await Groups.findByIdAndDelete(req.params.id)
        res.status(200).json("Group are deleted...")
    } catch (err) {
        res.status(500).json(err)
    }
});


// get User

router.get("/:id", verifyTokenAndAdmin, async (req, res) => {
    try {
        const groups = await Groups.findById(req.params.id)
        const { password, ...others } = groups._doc;
        res.status(200).json({...others});
    } catch (err) {
        res.status(500).json(err)
    }
});
// get all User

router.get("/find/all", verifyTokenAndAdmin, async (req, res) => {
    try {
        const groups = await Groups.find()

        res.status(200).json(groups);
    } catch (err) {
        res.status(500).json(err)
    }
});
module.exports = router
