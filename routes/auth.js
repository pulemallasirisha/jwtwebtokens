const router = require("express").Router();
const Groups = require("../models/groups");
const CryptoJS = require("crypto-js");
const jwt = require("jsonwebtoken");
const { verifyToken } = require("./verifyToken.js");
// const groups = require("../models/groups");
//Register
router.post("/register", async (req, res) => {
    const newgroups = new Groups({
        name: req.body.name,
        desc: req.body.desc,
        username: req.body.username,
        email: req.body.email,
        isAdmin:req.body.isAdmin,
        password: CryptoJS.AES.encrypt(req.body.password, process.env.PASS_SEC).toString(),


    });
    try {
        const saved = await newgroups.save();
        res.status(201).json(saved);
    } catch (err) {
        res.status(500).json(err);
    }
});


//login function

router.post("/login", async (req, res) => {
    try {
        const groups = await Groups.findOne({ username: req.body.username })
        !groups && res.status(401).json("wrong  credentials!");

        const hashedpassword = CryptoJS.AES.decrypt(
            groups.password,
            process.env.PASS_SEC
        );
        const originalpassword = hashedpassword.toString(CryptoJS.enc.Utf8)
        originalpassword !== req.body.password &&
            res.status(401).json("wrong password");
             //creating a access token
        const accessToken = jwt.sign({
            id: groups._id,
            isAdmin: groups.isAdmin,
        }, process.env.JWT_SEC);

        const { password, ...others } = groups._doc;
        res.status(200).json({ ...others, accessToken });
    }
    catch (err) {
        res.status(500).json(err)
    }

});
module.exports = router
